﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sudoku.Common;

namespace Sudoku.Core.Tests
{
    public class Helpers
    {
        /// <summary>
        /// Creates a new instance of a default alphabet.
        /// </summary>
        /// <returns>A new instance of an Alphabet with values 1 - 9.</returns>
        public static Alphabet CreateDefaultAlphabet()
        {
            return new Alphabet() {
                1, 2, 3, 4, 5, 6, 7, 8, 9
            };
        }
    }
}
