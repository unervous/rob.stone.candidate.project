﻿using System;
using System.Diagnostics;
using System.Runtime.Versioning;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sudoku.Common;
using Sudoku.Core.Tests.Properties;

namespace Sudoku.Core.Tests
{
    /// <summary>
    /// Summary description for StrategySolverTest
    /// </summary>
    [TestClass]
    public class StrategySolverTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        #region Helpers

        // Add helpers here

        #endregion Helpers

        [TestMethod]
        public void SolveBasicStrategyTest()
        {
            var file = Resources.Input;
            var puzzleValues = FileReader.GetPuzzle(file);
            var puzzle = new Puzzle(9, 9, 3, 3, Helpers.CreateDefaultAlphabet());
            puzzle.LoadPuzzle(puzzleValues);        
            var strategySolver = new StrategySolver();
            strategySolver.Strategies.Add(new BasicStrategy());

            strategySolver.Solve(puzzle);

            Assert.IsTrue(puzzle.IsSolved);
        }
    }
}
