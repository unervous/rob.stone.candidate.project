﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Sudoku.Common;
using Sudoku.Core;
using Sudoku.Web.Models;

namespace Sudoku.Web.Controllers
{
    public class PuzzleController : Controller
    {
        // GET: Puzzle
        public ActionResult Index()
        {
            var model = new PuzzleViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult FileUpload(PuzzleViewModel model)
        {
            if (!ModelState.IsValid) return View("Index", model);

            var file = model.File;
            var fileText = string.Empty;
            if (file != null && file.ContentLength > 0)
            {
                using (var reader = new StreamReader(file.InputStream))
                {
                    fileText = reader.ReadToEnd();
                }
            }

            if (!string.IsNullOrEmpty(fileText))
            {
                var puzzleValues = FileReader.GetPuzzle(fileText);
                model.Puzzle = new Puzzle(model.PuzzleWidth, model.PuzzleHeight, 3, 3, new Alphabet { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

                try
                {
                    model.Puzzle.LoadPuzzle(puzzleValues);
                    model.IsFileLoaded = true;
                }
                catch (InvalidPuzzleSizeException ex)
                {
                    ModelState.AddModelError("InvalidPuzzleSize", ex.ToString());
                }

                try
                {
                    model.Puzzle.Validate();          
                }
                catch (DuplicateValueException ex)
                {
                    model.IsFileLoaded = false;
                    ModelState.AddModelError("DuplicateValue", ex.ToString());
                }

                if (model.Puzzle.SolveCount == 0)
                {
                    model.IsFileLoaded = false;
                    ModelState.AddModelError("NoValues", "Your input file has no values");
                }
            }


            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Solve(PuzzleViewModel model, List<int> puzzleValues)
        {
            if (model != null && puzzleValues != null)
            {
                model.Alphabet = new Alphabet { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                model.Puzzle = new Puzzle(model.PuzzleWidth, model.PuzzleHeight, 3, 3, model.Alphabet);

                try
                {
                    model.Puzzle.LoadPuzzle(puzzleValues);
                }
                catch (InvalidPuzzleSizeException ex)
                {
                    ModelState.AddModelError("InvalidPuzzleSize", ex.ToString());
                }

                var solver = new StrategySolver();
                solver.Strategies.Add(new BasicStrategy());

                solver.Solve(model.Puzzle);

                try
                {
                    model.Puzzle.Validate();
                }
                catch (DuplicateValueException ex)
                {
                    ModelState.AddModelError("DuplicateValue", ex.ToString());
                }

                if (!model.Puzzle.IsSolved)
                    ModelState.AddModelError("PuzzleNotSolved", "The puzzle was not solved");
            }

            return View("Index", model);
        }
    }
}
