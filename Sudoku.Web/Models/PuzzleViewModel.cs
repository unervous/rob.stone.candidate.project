﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Sudoku.Common;

namespace Sudoku.Web.Models
{
    public class PuzzleViewModel
    {
        [Required]
        [Range(4, 9, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int PuzzleWidth { get; set; }

        [Required]
        [Range(4, 9, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int PuzzleHeight { get; set; }

        public Alphabet Alphabet { get; set; }

        [Required]
        public HttpPostedFileBase File { get; set; }

        public Puzzle Puzzle { get; set; }

        public bool IsFileLoaded { get; set; }
    }
}