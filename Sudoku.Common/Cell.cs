﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using NightRays;

namespace Sudoku.Common
{
    public class Cell
    {
        #region Properties

        private readonly Alphabet _RemainingPossibilities;
        /// <summary>
        /// Gets the remaining possible values this cell can have.
        /// </summary>
        protected Alphabet RemainingPossibilities
        {
            get { return _RemainingPossibilities; }
        }

        private readonly ICellObserver _CellObserver;
        /// <summary>
        /// Gets the observer of this cell.
        /// </summary>
        public ICellObserver CellObserver
        {
            get { return _CellObserver; }
        }

        /// <summary>
        /// Gets whether the cell value has been determined.
        /// </summary>
        public bool IsSolved
        {
            get { return this.RemainingPossibilities.Count == 1; }
        }
        /// <summary>
        /// Gets or sets the solved value of this cell.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets the puzzle column index of this cell.
        /// </summary>
        public int PuzzleColumnIndex { get; set; }

        /// <summary>
        /// Gets or sets the puzzle row index of this cell.
        /// </summary>
        public int PuzzleRowIndex { get; set; }

        /// <summary>
        /// Gets or sets the puzzle box index of this cell.
        /// </summary>
        public int PuzzleBoxIndex { get; set; }

        public bool IsCellLocked { get; private set; }

        #endregion Properties

        #region Construction

        /// <summary>
        /// Initializes a new instance of the Sudoku.Common.Cell class
        /// with the specified puzzle alphabet.
        /// </summary>
        /// <param name="alphabet">The collection of all possible cell values.</param>
        public Cell(Alphabet alphabet)
        {
            _RemainingPossibilities = alphabet.Clone();
        }

        /// <summary>
        /// Initializes a new instance of the Sudoku.Common.Cell class
        /// with the specified puzzle alphabet and observer.
        /// </summary>
        /// <param name="alphabet">The collection of all possible cell values.</param>
        /// <param name="cellObserver">The observer of this cell.</param>
        public Cell(Alphabet alphabet, ICellObserver cellObserver)
        {
            _CellObserver = cellObserver;
            _RemainingPossibilities = alphabet.Clone();
        }

        #endregion Construction

        #region Methods

        /// <summary>
        /// Determines whether the specified value is a remaining possible value.
        /// </summary>
        /// <param name="value">The possible value.</param>
        /// <returns>True if this cell can have this specified value.</returns>
        public bool CanBe(int value)
        {
            return this.RemainingPossibilities.Contains(value);
        }

        /// <summary>
        /// Removes the specified value from the cell's collection of possible values.
        /// </summary>
        /// <param name="value">The possible value to remove.</param>
        public void RemovePossibility(int value)
        {
            if(IsCellLocked) return;

            if (this.RemainingPossibilities.Contains(value))
            {
                this.RemainingPossibilities.Remove(value);

                if (RemainingPossibilities.Count == 1) 
                    Value = RemainingPossibilities.First();

                OnCellChanged();
                if (this.IsSolved)
                    OnCellSolved();
            }
        }

        public static void RemovePossibility(IList<Cell> cells, int value)
        {
            foreach (var cell in cells.Where(c => !c.IsSolved))
            {
                cell.RemovePossibility(value);
            }
        }

        /// <summary>
        /// Sets the solved value of the cell and locks the cell so it can not be changed.
        /// </summary>
        /// <param name="value">Set the solved cell value to value.</param>
        public void SetDefaultCellValue(int value)
        {
            this.Value = value;
            this.IsCellLocked = true;

            RemainingPossibilities.Clear();
            RemainingPossibilities.Add(value);
            OnCellSolved();
        }

        /// <summary>
        /// Adds the specified value to the cell's collection of possible values.
        /// </summary>
        /// <param name="value">The possible value to add.</param>
        public void AddPossibility(int value)
        {
            if (IsCellLocked) return;
            if (this.RemainingPossibilities.Contains(value)) return;
            
            this.RemainingPossibilities.Add(value);
            OnCellChanged();
        }

        public override string ToString()
        {
            if (this.IsSolved)
                return this.Value.ToString();
            else
                return string.Format("({0})", this.RemainingPossibilities.Implode(" ", v => v.ToString()));
        }

        #endregion Methods

        #region Event Handling

        /// <summary>
        /// Raises the CellChanged event.
        /// </summary>
        protected virtual void OnCellChanged()
        {
            if(this.CellObserver != null)
                this.CellObserver.ReportCellChanged(this);
        }

        /// <summary>
        /// Raises the CellSolved event.
        /// </summary>
        protected virtual void OnCellSolved()
        {
            if (this.CellObserver != null)
                this.CellObserver.ReportCellSolved(this);
        }

        #endregion Event Handling
    }
}