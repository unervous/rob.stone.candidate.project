﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sudoku.Common
{
    public class Row : Feature
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the Sudoku.Common.Column class
        /// with its specified position and associated puzzle.
        /// </summary>
        public Row(int position, Puzzle puzzle)
            : base(position)
        {
            var row = puzzle.Cells.Skip(position * puzzle.Width).Take(puzzle.Width).ToList();

            foreach (var cell in row)
            {
                cell.PuzzleRowIndex = position;
                Cells.Add(cell);
            }
        }

        #endregion Construction
    }
}
