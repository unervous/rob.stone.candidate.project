﻿using System;
using System.Collections.Generic;
using NightRays;

namespace Sudoku.Common
{
    /// <summary>
    /// Represents a complete collection of possible puzzle values.
    /// </summary>
    public class Alphabet : List<int>
    {
        /// <summary>
        /// Creates a new instance of itself with the same values.
        /// </summary>
        /// <returns></returns>
        public Alphabet Clone()
        {
            var clone = new Alphabet();
            var list = (Alphabet) MemberwiseClone();
            clone.AddRange(list);
            return clone;
        }

        public override string ToString()
        {
            return this.Implode(" ", a => a.ToString());
        }
    }
}
