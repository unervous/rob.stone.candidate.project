﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sudoku.Common
{
    public class InvalidPuzzleSizeException : Exception
    {
        /// <summary>
        /// Gets the puzzle that contains the duplicate value.
        /// </summary>
        public Puzzle Puzzle { get; private set; }

        public List<int> PuzzleValues { get; private set; }

        #region Construction

        /// <summary>
        /// Initializes a new instance of the Sudoku.Common.DuplicateValueException class
        /// with the specified puzzle and puzzle values.
        /// </summary>
        /// <param name="puzzle">The puzzle.</param>
        /// <param name="puzzleValues">The loaded puzzle values.</param>
        public InvalidPuzzleSizeException(Puzzle puzzle, List<int> puzzleValues)
        {
            Puzzle = puzzle;
            PuzzleValues = puzzleValues;
        }

        #endregion Construction
        
        #region Methods

        public override string ToString()
        {
           return string.Format("Your puzzle is {0}x{1} so you should have {2} values but your file only had {3} values",
                    Puzzle.Width,
                    Puzzle.Height,
                    Puzzle.Width * Puzzle.Height,
                    PuzzleValues.Count); 
        }

        #endregion Methods
    }
}
