﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Sudoku.Common
{
    public class FileReader
    {
        public static List<int> GetPuzzleFromFile(string path)
        {
            string text;
            using (var streamReader = new StreamReader(path))
            {
                text = streamReader.ReadToEnd();
            }
            return GetPuzzle(text);
        }

        public static List<int> GetPuzzle(string text)
        {
            var stripped = Regex.Replace(text, @"[^\d\.]", "");
            var values = new List<int>();

            var charArray = stripped.ToCharArray();
            foreach (var character in charArray.Select(c => c.ToString()))
            {
                if (character == ".")
                {
                    values.Add(0);
                }
                else
                {
                    var value = 0;
                    if (int.TryParse(character, out value))
                    {
                        values.Add(value);
                    }
                }
            }

            return values;
        }

        public static string GetAppPath()
        {
            var path = string.Empty;
            var directoryInfo = new DirectoryInfo(Environment.CurrentDirectory);
            if (directoryInfo.Parent != null && directoryInfo.Parent.Parent != null)
            {
                path = directoryInfo.Parent.Parent.FullName;
            }

            return path;
        }
    }
}
