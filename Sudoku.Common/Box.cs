﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sudoku.Common
{
    /// <summary>
    /// Represents a puzzle box that knows references to cells and its position within a puzzle.
    /// </summary>
    public class Box : Feature
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the Sudoku.Common.Box class
        /// with its specified position and associated puzzle.
        /// </summary>
        public Box(int position, Puzzle puzzle)
            : base(position)
        {
            var boxStartIndexes = BoxStartIndexes(puzzle);
            var startIndex = boxStartIndexes[position];

            for (int i = 0; i < puzzle.BoxWidth; i++)
            {
                var boxRow = puzzle.Cells.Skip(startIndex).Take(puzzle.BoxWidth).ToList();

                foreach (var cell in boxRow)
                {
                    cell.PuzzleBoxIndex = position;
                    Cells.Add(cell);
                }

                startIndex = startIndex + puzzle.Width;
            }          
        }

        public static List<int> BoxStartIndexes(Puzzle puzzle)
        {
            var boxIndexes = new List<int>();
            for (int i = 0; i < puzzle.Width / puzzle.BoxWidth; i++)
            {
                boxIndexes.Add(i * puzzle.BoxWidth);
                for (int j = 1; j < puzzle.Height / puzzle.BoxHeight; j++)
                {
                    boxIndexes.Add((j * puzzle.Width * puzzle.BoxWidth) + (i * puzzle.BoxWidth));
                }
            }
            boxIndexes.Sort();
            
            return boxIndexes;
        }

        #endregion Construction
    }
}
