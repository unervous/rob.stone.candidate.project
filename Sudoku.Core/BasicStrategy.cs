﻿using System.Linq;
using Sudoku.Common;

namespace Sudoku.Core
{
    public class BasicStrategy : Strategy
    {
        public override void ApplyStrategy(Puzzle puzzle)
        {          
            var unsolvedCells = puzzle.Cells.Where(c => !c.IsSolved).ToArray();

            foreach (var unsolvedCell in unsolvedCells)
            {
                var solvedColumnValues = puzzle.Columns[unsolvedCell.PuzzleColumnIndex]
                                                .Cells.Where(c => c.IsSolved)
                                                .Select(c => c.Value)
                                                .ToArray();

                var solvedRowValues = puzzle.Rows[unsolvedCell.PuzzleRowIndex]
                                                .Cells.Where(c => c.IsSolved)
                                                .Select(c => c.Value)
                                                .ToArray();

                var solvedBoxValues = puzzle.Boxes[unsolvedCell.PuzzleBoxIndex]
                                                .Cells.Where(c => c.IsSolved)
                                                .Select(c => c.Value)
                                                .ToArray();

                var values = solvedColumnValues.Union(solvedRowValues).Union(solvedBoxValues).ToList();
                values.Sort();

                foreach (var value in values)
                {
                    unsolvedCell.RemovePossibility(value); 
                }
                
                if (unsolvedCell.IsSolved)
                    ApplyStrategy(puzzle);
            }
        }
    }
}
