﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Sudoku.Core;
using Sudoku.Common;

namespace Sudoku.ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Run();
        }

        private static void Run()
        {
            var input = new ConsoleInput();
            input.Start();

            StrategySolver solver = new StrategySolver();
            solver.Strategies.Add(new BasicStrategy());

            var puzzle = new Puzzle(input.Width, input.Height, 3, 3, input.Alphabet);

            try
            {
                puzzle.LoadPuzzle(input.PuzzleValues);
            }
            catch (InvalidPuzzleSizeException ex)
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid puzzle file format");
                Console.ResetColor();
                Console.WriteLine(ex.ToString());
                Console.WriteLine();
                Restart();
            }

            try
            {
                puzzle.Validate();
            }
            catch (DuplicateValueException ex)
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Puzzle has duplicate values");
                Console.ResetColor();
                Console.WriteLine(ex.ToString());
                Console.WriteLine();
                Restart();
            }

            Console.Clear();
            Console.WriteLine();
            PuzzleFormater.ShowPuzzle(puzzle);

            if (puzzle.SolveCount == 0)
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There were no solved values for the loaded file");
                Console.ResetColor();
                Restart();
            }

            Console.WriteLine();
            Console.WriteLine("Press <ENTER> to solve the puzzle.");
            Console.ReadLine();

            solver.Solve(puzzle);
            PuzzleFormater.ShowPuzzle(puzzle);

            if (!puzzle.IsSolved)
            {
                Console.WriteLine();
                ConsoleInput.WriteError("Unable to solve the puzzle");
            }

            Console.WriteLine();
            Console.WriteLine("Press <ENTER> to continue.");
            Console.ReadLine();
        }

        private static void Restart()
        {
            Console.WriteLine("Press <ENTER to try again>");
            Console.ReadLine();
            Console.Clear();
            Run();
        }
    }
}
