﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Sudoku.Common;

namespace Sudoku.ConsoleApplication
{
    public class ConsoleInput
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public string PuzzleDirectory { get; set; }

        public List<int> PuzzleValues { get; set; }

        public Alphabet Alphabet { get; set; }

        public ConsoleInput()
        {
            Width = 0;
            Height = 0;
            PuzzleValues = new List<int>();
            Alphabet = new Alphabet { 1,2,3,4,5,6,7,8,9 };
        }

        public void Start()
        {
            GetPuzzleColumn();
            GetPuzzleRow();
            GetPuzzleValues();
        }

        public void GetPuzzleColumn()
        {
            int width;

            Console.WriteLine();
            Console.WriteLine("Please enter the number of columns: ");
            var numOfColumns = Console.ReadLine();
            int.TryParse(numOfColumns, out width);

            while (width == 0)
            {
                Console.WriteLine();
                WriteError(string.Format("{0} is not a valid number", numOfColumns));
                Console.WriteLine("Please enter the NUMBER of columns: ");
                numOfColumns = Console.ReadLine();
                int.TryParse(numOfColumns, out width);
            }

            Width = width;
        }

        public void GetPuzzleRow()
        {
            int height;

            Console.WriteLine();
            Console.WriteLine("Please enter the number of rows: ");
            var numOfRows = Console.ReadLine();
            int.TryParse(numOfRows, out height);

            while (height == 0)
            {
                Console.WriteLine();
                WriteError(string.Format("{0} is not a valid number", numOfRows));
                Console.WriteLine("Please enter the NUMBER of rows: ");
                numOfRows = Console.ReadLine();
                int.TryParse(numOfRows, out height);
            }

            Height = height;
        }

        public void GetAlphabet()
        {
            Console.WriteLine();
            Console.WriteLine("Enter your comma separated alphabet or press <ENTER> to use 1-9");
            var alphabet = Console.ReadLine();

            if (!string.IsNullOrEmpty(alphabet))
            {

            }
        }

        public void GetPuzzleValues()
        {
            var filePaths = new List<string>();
            var filesExist = true;

            while (!filePaths.Any())
            {
                string path = GetPuzzleDirectory(filesExist);
                try
                {
                    filePaths = Directory.GetFiles(path, "*.txt").ToList();
                }
                catch (DirectoryNotFoundException)
                {
                    WriteError("Directory not found");
                }
                catch (Exception ex)
                {
                    WriteError(ex.Message);
                }
                
                
                filesExist = filePaths.Count > 0;
            }

            var fileNumberToLoad = 0;
            while (fileNumberToLoad == 0 || fileNumberToLoad >= (filePaths.Count + 1))
            {
                Console.WriteLine();
                if (fileNumberToLoad >= (filePaths.Count + 1))
                {
                    WriteError(string.Format("{0} is not a valid option", fileNumberToLoad));
                }
                Console.WriteLine("Enter the number for the file you would like to load");
                
                for (var i = 1; i <= filePaths.Count(); i++)
                {
                    var fileName = filePaths[i - 1].Split('\\').Last();
                    Console.WriteLine("{0} {1}", i, fileName);
                }
                Console.WriteLine();
                
                var fileNumberInput = Console.ReadLine();
                if (!int.TryParse(fileNumberInput, out fileNumberToLoad))
                {
                    Console.WriteLine();
                    WriteError(string.Format("{0} is not a valid number", fileNumberInput));
                    Console.WriteLine("Enter the NUMBER for the file you would like to load");
                }
            }

            PuzzleValues = FileReader.GetPuzzleFromFile(filePaths[fileNumberToLoad - 1]);
        }

        public string GetPuzzleDirectory(bool filesExist)
        {
            var path = string.Empty;

            while (string.IsNullOrEmpty(path))
            {
                Console.WriteLine();

                if (!filesExist)
                {
                    WriteWarning(string.Format("0 txt files in {0}", PuzzleDirectory));
                    Console.WriteLine(@"Example: C:\Directory\PuzzleDirectory");
                }

                Console.WriteLine("Enter the path for the director where your puzzle files are located:");
                path = Console.ReadLine();
            }
            
            PuzzleDirectory = path;
            return path;
        }

        public static void WriteError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0}", message);
            Console.ResetColor();
        }

        public static void WriteWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("{0}", message);
            Console.ResetColor();
        }
    }
}
