﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sudoku.Common;

namespace Sudoku.ConsoleApplication
{
    public class PuzzleFormater
    {
        public static void ShowPuzzle(Puzzle puzzle)
        {
            Console.WriteLine();
            Console.WriteLine();

            if(puzzle.IsSolved)
                Console.ForegroundColor = ConsoleColor.Cyan;

            foreach (var row in puzzle.Rows)
            {
                PuzzleRow(row);
            }
            Console.ResetColor();
        }

        private static void PuzzleRow(Row row)
        {
            var sb = new StringBuilder();
            foreach (var cell in row.Cells)
            {              
                sb.Append(string.Format("{0,3}", cell.Value));          
            }
            Console.WriteLine(sb.ToString());
        }
    }
}
