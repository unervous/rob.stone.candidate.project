﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sudoku.Common.Tests
{
    [TestClass()]
    public class RowTest
    {
        #region Test Attributes

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion Test Attributes

        #region Helpers

        /// <summary>
        /// Creates a new instance of a default alphabet.
        /// </summary>
        /// <returns>A new instance of an Alphabet with values 1 - 9.</returns>
        public Alphabet CreateDefaultAlphabet()
        {
            return new Alphabet() {
                1, 2, 3, 4, 5, 6, 7, 8, 9
            };
        }

        #endregion Helpers

        #region Tests

        /// <summary>
        ///A test for Row Constructor
        ///</summary>
        [TestMethod()]
        public void RowConstructorTest()
        {
            int position = 3;
            int width = 9;
            int height = 9;
            int boxWidth = 3;
            int boxHeight = 3;
            Alphabet alphabet = Helpers.CreateDefaultAlphabet();
            Puzzle puzzle = new Puzzle(width, height, boxWidth, boxHeight, alphabet);
            Row target = new Row(position, puzzle);

            Assert.IsNotNull(target);
            Assert.AreEqual(width, target.Cells.Count);

            for (int i = 0; i < width; i++)
            {
                Debug.WriteLine(string.Format("Position: {0}", i));
                foreach (var cell in puzzle.Rows[i].Cells)
                {
                    Debug.WriteLine(
                        string.Format("     Row Index: {0} Column Index: {1} Box Index: {2}",
                        cell.PuzzleRowIndex, cell.PuzzleColumnIndex, cell.PuzzleBoxIndex));
                    Assert.AreEqual(i, cell.PuzzleRowIndex);
                }
            }
        }

        [TestMethod]
        public void CellSolvedTest()
        {
            var position = 1;
            var value = 1;
            var puzzle = new Puzzle(9, 9, 3, 3, CreateDefaultAlphabet());
            var target = new Row(position, puzzle);

            var cell = target.Cells.First();
            cell.SetDefaultCellValue(value);

            target.CellSolved(cell);

            foreach (var c in target.Cells.Where(c => c.IsSolved == false))
            {
                Assert.IsFalse(c.CanBe(value));
            }
        }

        [TestMethod()]
        public void RowLogicNineByNineTest()
        {
            var position = 8;
            int width = 9;
            List<int> cells = new List<int> { 1,  2,  3,  4,  5,  6,  7,  8,  9, 
                                             10, 11, 12, 13, 14, 15, 16, 17, 18,
                                             19, 20, 21, 22, 23, 24, 25, 26, 27,
                                             28, 29, 30, 31, 32, 33, 34, 35, 36,
                                             37, 38, 39, 40, 41, 42, 43, 44, 45,
                                             46, 47, 48, 49, 50, 51, 52, 53, 54,
                                             55, 56, 57, 58, 59, 60, 61, 62, 63,
                                             64, 65, 66, 67, 68, 69, 70, 71, 72,
                                             73, 74, 75, 76, 77, 78, 79, 80, 81};

            var row = cells.Skip(position * width).Take(width).ToList();

            Assert.IsNotNull(row);
            Assert.AreEqual(width, row.Count);
        }

        #endregion Tests
    }
}
