﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sudoku.Common.Tests.Properties;

namespace Sudoku.Common.Tests
{
    [TestClass()]
    public class PuzzleTest
    {
        #region Test Attributes

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion Test Attributes

        #region Helpers

        /// <summary>
        /// Creates a new instance of a default alphabet.
        /// </summary>
        /// <returns>A new instance of an Alphabet with values 1 - 9.</returns>
        public Alphabet CreateDefaultAlphabet()
        {
            return new Alphabet {
                1, 2, 3, 4, 5, 6, 7, 8, 9
            };
        }

        #endregion Helpers

        #region Tests

        /// <summary>
        ///A test for Box Constructor
        ///</summary>
        [TestMethod]
        public void PuzzleConstructorTest()
        {
            int width = 9;
            int height = 9;
            int boxWidth = 3;
            int boxHeight = 3;
            Alphabet alphabet = CreateDefaultAlphabet();
            Puzzle puzzle = new Puzzle(width, height, boxWidth, boxHeight, alphabet);

            Assert.IsNotNull(puzzle);
            Assert.AreEqual(width, puzzle.Width);
            Assert.AreEqual(height, puzzle.Height);
            Assert.AreEqual(boxWidth, puzzle.BoxWidth);
            Assert.AreEqual(boxHeight, puzzle.BoxHeight);
            Assert.AreEqual(0, puzzle.SolveCount);
            Assert.IsFalse(puzzle.IsSolved);
      
            for (int i = 0; i < width; i++)
            {
                foreach (var cell in puzzle.Rows[i].Cells)
                {
                    Assert.AreEqual(i, cell.PuzzleRowIndex);
                }

                foreach (var cell in puzzle.Columns[i].Cells)
                {
                    Assert.AreEqual(i, cell.PuzzleColumnIndex);
                }

                foreach (var cell in puzzle.Boxes[i].Cells)
                {
                    Assert.AreEqual(i, cell.PuzzleBoxIndex);
                }
            }
        }

        [TestMethod]
        public void PuzzleCellTest()
        {
            int value = 4;
            int width = 9;
            int height = 9;
            int boxWidth = 3;
            int boxHeight = 3;
            Alphabet alphabet = CreateDefaultAlphabet();
            Puzzle puzzle = new Puzzle(width, height, boxWidth, boxHeight, alphabet);

            var cell = puzzle[0, 0];
            cell.Value = value;

            var cellFromCells = puzzle.Cells[0];
            var cellFromRow = puzzle.Rows[0].Cells[0];
            var cellFromRowAndColumn = puzzle[0, 0];
            var cellFromColumn = puzzle.Columns[0].Cells[0];

            Assert.IsNotNull(puzzle);
            Assert.AreEqual(value, cellFromCells.Value);        // Cell references correct puzzle cell
            Assert.AreEqual(value, cellFromRow.Value);          // Row cell references correct puzzle cell
            Assert.AreEqual(value, cellFromRowAndColumn.Value); // Puzzle cell references correct cell
            Assert.AreEqual(value, cellFromColumn.Value);       // Column cell referenees correct puzzle cell
        }

        [TestMethod]
        public void PuzzleLoadTest()
        {
            List<int> input = new List<int> { 2,  0,  0,  4,  5,  0,  8,  0,  0, 
                                              0,  9,  0,  0,  3,  0,  1,  0,  2,
                                              0,  7,  6,  0,  0,  2,  0,  4,  0,
                                              3,  0,  0,  5,  7,  0,  0,  2,  0,
                                              0,  5,  0,  0,  0,  8,  3,  0,  4,
                                              7,  0,  4,  0,  0,  1,  0,  8,  0,
                                              0,  1,  0,  2,  8,  0,  0,  0,  6,
                                              6,  0,  3,  7,  0,  0,  0,  5,  0,
                                              0,  0,  8,  0,  0,  3,  7,  0,  9};

            Puzzle puzzle = new Puzzle(9, 9, 3, 3, CreateDefaultAlphabet());
            puzzle.LoadPuzzle(input);


            foreach (var cell in puzzle.Cells)
            {
                if (cell.IsCellLocked)
                {
                    Assert.IsTrue(cell.IsSolved);           // Cell should be solved
                    Assert.IsTrue(cell.CanBe(cell.Value));  // Cell can be the solved value
                    Assert.IsTrue(cell.Value != 0);         // Cell value should not be 0
                }
                else
                {
                    Assert.IsFalse(cell.IsCellLocked);       // Cell should not be locked
                }
            }
        }

        [TestMethod]
        public void PuzzleIsSolvedTest()
        {
            var file = Resources.Input;
            var puzzleValues = FileReader.GetPuzzle(file);
            var puzzle = new Puzzle(9, 9, 3, 3, CreateDefaultAlphabet());
            
            puzzle.LoadPuzzle(puzzleValues);
            puzzle.Validate();

            Assert.IsFalse(puzzle.IsSolved);
        }

        #endregion Tests
    }
}
