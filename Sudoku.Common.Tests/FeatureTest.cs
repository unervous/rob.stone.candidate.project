﻿using System.Diagnostics;
using System.Linq;
using Sudoku.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Sudoku.Common.Tests
{
    /// <summary>
    ///This is a test class for FeatureTest and is intended
    ///to contain all FeatureTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FeatureTest
    {
        #region Test Attributes

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion Test Attributes

        #region Helpers

        /// <summary>
        /// Creates a new instance of a default alphabet.
        /// </summary>
        /// <returns>A new instance of an Alphabet with values 1 - 9.</returns>
        public Alphabet CreateDefaultAlphabet()
        {
            return new Alphabet() {
                1, 2, 3, 4, 5, 6, 7, 8, 9
            };
        }

        #endregion Helpers

        #region Tests

        /// <summary>
        ///A test for Feature Constructor
        ///</summary>
        [TestMethod()]
        public void FeatureConstructorTest()
        {
            int position = 3;
            Feature target = new Feature(position);
            
            Assert.IsNotNull(target);
            Assert.AreEqual(position, target.Position);     // value setting successful
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        public void ToStringTest()
        {
            int position = 3;
            var alphabet = CreateDefaultAlphabet();
            Feature target = new Feature(position);
            target.Cells.Add(new Cell(alphabet));
            string expected = "(1 2 3 4 5 6 7 8 9)";
            string actual = target.ToString();
            
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Validate
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(DuplicateValueException))]
        public void ValidateTest()
        {
            var position = 0;
            var alphabet = CreateDefaultAlphabet();
            var target = new Feature(position);
            var cell = new Cell(alphabet);
            const int solvedValue = 1;
           
            foreach (var value in alphabet.Where(v => v != solvedValue))
            {
                cell.RemovePossibility(value);
            }

            target.Cells.Add(cell);
            target.Cells.Add(cell);

            var exception = new DuplicateValueException(target, solvedValue);
            var exceptionMessage = exception.ToString();

            try
            {
                target.Validate();
            }
            catch (DuplicateValueException ex)
            {
                var message = ex.ToString();
                Assert.AreEqual(exceptionMessage, message);
                throw;
            }
        }

        /// <summary>
        ///A test for Cells
        ///</summary>
        [TestMethod()]
        public void CellsTest()
        {
            int position = 0;
            Feature target = new Feature(position);
            List<Cell> cells = new List<Cell>
            {
                new Cell(CreateDefaultAlphabet()),
                new Cell(CreateDefaultAlphabet())
            };
            foreach (var cell in cells)
            {
                target.Cells.Add(cell);
            }
            
            var actual = target.Cells;

            Assert.IsNotNull(target.Cells);
            Assert.AreEqual(cells.Count, actual.Count);
        }

        /// <summary>
        ///A test for Position
        ///</summary>
        [TestMethod()]
        public void PositionTest()
        {
            int position = 1000;
            Feature target = new Feature(position);
            
            var actual = target.Position;
            
            Assert.AreEqual(position, actual);
        }

        #endregion Tests
    }
}