﻿using System.Collections.Generic;
using System.Linq;
using Sudoku.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Sudoku.Common.Tests
{
    /// <summary>
    /// This is a test class for CellTest and is intended
    /// to contain all CellTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CellTest
    {
        #region Test Attributes

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion Test Attributes

        #region Helpers

        // Add test helpers here

        #endregion Helpers

        #region Tests

        /// <summary>
        ///A test for Cell Constructor
        ///</summary>
        [TestMethod()]
        public void CellConstructorTest()
        {
            /*** ARRANGE ***/
            Alphabet alphabet = Helpers.CreateDefaultAlphabet();

            /*** ACT ***/
            Cell target = new Cell(alphabet);

            /*** ASSERT ***/
            Assert.IsNotNull(target);
        }

        /// <summary>
        /// A test for RemovePossibility to verify that removed values can no longer be possible values 
        /// without affecting the specified alphabet.
        /// </summary>
        [TestMethod()]
        public void RemovePossibilityTest()
        {
            /*** ARRANGE ***/
            Alphabet alphabet = Helpers.CreateDefaultAlphabet();
            Cell target = new Cell(alphabet);
            int value = 3;                              // arbitrary

            /*** ACT ***/
            target.RemovePossibility(value);

            /*** ASSERT ***/
            Assert.IsFalse(target.CanBe(value));        // value is no longer a possible value
            Assert.IsTrue(alphabet.Contains(value));    // alphabet remains unchanged
        }

        /// <summary>
        /// A test for RemovePossibility to verify that removed values can no longer be possible values 
        /// without affecting the specified alphabet.
        /// </summary>
        [TestMethod()]
        public void RemovePossibilityFromCellsTest()
        {
            /*** ARRANGE ***/
            var alphabet = Helpers.CreateDefaultAlphabet();
            var cells = new List<Cell>
            {
                new Cell(alphabet),
                new Cell(alphabet),
                new Cell(alphabet)
            };

            const int value = 3; // arbitrary

            /*** ACT ***/
            Cell.RemovePossibility(cells, value);

            /*** ASSERT ***/
            foreach (var cell in cells)
            {
                Assert.IsFalse(cell.CanBe(value));        // value is no longer a possible value
                Assert.IsTrue(alphabet.Contains(value));    // alphabet remains unchanged
            }         
        }

        /// <summary>
        /// A test for RemovePossibility to verify that removed values can no longer be possible values 
        /// without affecting the specified alphabet.
        /// </summary>
        [TestMethod()]
        public void AddPossibilityTest()
        {
            /*** ARRANGE ***/
            Alphabet alphabet = new Alphabet() { 1, 3, 5 };
            Cell target = new Cell(alphabet);
            int value = 3;                              // arbitrary

            /*** ACT ***/
            target.AddPossibility(value);

            /*** ASSERT ***/
            Assert.IsTrue(target.CanBe(value));        // value is no longer a possible value
            Assert.IsTrue(alphabet.Contains(value));   // alphabet remains unchanged

            value = 2;
            
            target.AddPossibility(value);

            Assert.IsTrue(target.CanBe(value));        // value is no longer a possible value
            Assert.IsFalse(alphabet.Contains(value));  // alphabet remains unchanged
        }

        /// <summary>
        /// A test for CanBe
        /// </summary>
        [TestMethod()]
        public void CanBeTest()
        {
            Alphabet alphabet = Helpers.CreateDefaultAlphabet();
            Cell target = new Cell(alphabet);
            const int value = 3; 
                      
            bool actual = target.CanBe(value);
            
            Assert.IsTrue(actual);
            
            target.RemovePossibility(value);
            target.Value = value;

            Assert.IsFalse(target.CanBe(value));
        }

        /// <summary>
        ///A test for IsSolved
        ///</summary>
        [TestMethod()]
        public void IsSolvedTest()
        {
            Alphabet alphabet = Helpers.CreateDefaultAlphabet();
            Cell target = new Cell(alphabet); 
            const int value = 3;

            bool actual = target.IsSolved;
            
            Assert.IsFalse(actual);

            foreach (var a in alphabet.Where(a => a != value))
            {
                target.RemovePossibility(a);
            }

            actual = target.IsSolved;

            Assert.IsTrue(actual);
        }

        /// <summary>
        /// A test for setting the cell's Value.
        /// </summary>
        [TestMethod()]
        public void ValueTest()
        {
            /*** ARRANGE ***/
            Alphabet alphabet = Helpers.CreateDefaultAlphabet();
            Cell target = new Cell(alphabet);
            int expected = 3;                   // arbitrary
            int actual;

            /*** ACT ***/
            target.Value = expected;

            /*** ASSERT ***/
            actual = target.Value;
            Assert.AreEqual(expected, actual);  // value setting successful

            alphabet.Remove(expected);          // cell can be no other value
            foreach (int value in alphabet)
            {
                target.RemovePossibility(value);
                Assert.IsFalse(target.CanBe(value));
            }
        }

        #endregion Tests
    }
}