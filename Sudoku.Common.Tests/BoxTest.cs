﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sudoku.Common.Tests
{
    /// <summary>
    ///This is a test class for BoxTest and is intended
    ///to contain all BoxTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BoxTest
    {
        #region Test Attributes

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion Test Attributes

        #region Helpers

        /// <summary>
        /// Creates a new instance of a default alphabet.
        /// </summary>
        /// <returns>A new instance of an Alphabet with values 1 - 9.</returns>
        public Alphabet CreateDefaultAlphabet()
        {
            return new Alphabet() {
                1, 2, 3, 4, 5, 6, 7, 8, 9
            };
        }

        public static List<int> BoxStartIndexes(int width, int height, int boxWidth, int boxHeight)
        {
            var boxIndexes = new List<int>();
            for (int i = 0; i < width / boxWidth; i++)
            {
                boxIndexes.Add(i * boxWidth);
                for (int j = 1; j < height / boxHeight; j++)
                {
                    boxIndexes.Add((j * width * boxWidth) + (i * boxWidth));
                }
            }
            boxIndexes.Sort();

            return boxIndexes;
        }

        public static List<T> GetBoxCells<T>(List<T> cells, int position, int width, int boxWidth, int height, int boxHeight)
        {
            var box = new List<T>();
            var boxStartIndexes = BoxStartIndexes(width, height, boxWidth, boxHeight);
            var startIndex = boxStartIndexes[position];

            if (startIndex % width == 0)
                startIndex = position * width;

            for (int i = 0; i < boxWidth; i++)
            {
                var boxRow = cells.Skip(startIndex).Take(boxWidth).ToList();
                boxRow.ForEach(box.Add);
                startIndex = startIndex + width;
            }

            return box;
        }

        #endregion Helpers

        #region Tests

        /// <summary>
        ///A test for Box Constructor
        ///</summary>
        [TestMethod()]
        public void BoxConstructorTest()
        {
            int position = 3;
            int width = 9;
            int height = 9;
            int boxWidth = 3;
            int boxHeight = 3;
            Alphabet alphabet = Helpers.CreateDefaultAlphabet();
            Puzzle puzzle = new Puzzle(width, height, boxWidth, boxHeight, alphabet);
            Box target = new Box(position, puzzle);

            Assert.IsNotNull(target);
            Assert.AreEqual(boxWidth * boxHeight, target.Cells.Count);

            for (int i = 0; i < width; i++)
            {
                Debug.WriteLine(string.Format("Position: {0}", i));
                foreach (var cell in puzzle.Boxes[i].Cells)
                {
                    Debug.WriteLine(
                        string.Format("     Row Index: {0} Column Index: {1} Box Index: {2}",
                        cell.PuzzleRowIndex, cell.PuzzleColumnIndex, cell.PuzzleBoxIndex));
                    
                    Assert.AreEqual(i, cell.PuzzleBoxIndex);
                }
            }
        }

        // Created these tests so I could test how box cells are found
        #region Box Logic Tests

        [TestMethod]
        [Ignore]
        public void BoxLogicNineByNineTest()
        {
            int position = 4;
            int width = 9;
            int height = 9;
            int boxWidth = 3;
            int boxHeight = 3;
            List<int> cells = new List<int> { 1,  2,  3,  4,  5,  6,  7,  8,  9, 
                                             10, 11, 12, 13, 14, 15, 16, 17, 18,
                                             19, 20, 21, 22, 23, 24, 25, 26, 27,
                                             28, 29, 30, 31, 32, 33, 34, 35, 36,
                                             37, 38, 39, 40, 41, 42, 43, 44, 45,
                                             46, 47, 48, 49, 50, 51, 52, 53, 54,
                                             55, 56, 57, 58, 59, 60, 61, 62, 63,
                                             64, 65, 66, 67, 68, 69, 70, 71, 72,
                                             73, 74, 75, 76, 77, 78, 79, 80, 81};
            
            var box = GetBoxCells<int>(cells, position, width, boxWidth, height, boxHeight);

            Assert.IsNotNull(box);
            Assert.AreEqual(boxWidth * boxHeight, box.Count);
        }

        [TestMethod()]
        [Ignore]
        public void BoxLogicFourByFourTest()
        {
            int position = 2;
            int width = 4;
            int height = 4;
            int boxWidth = 2;
            int boxHeight = 2;
            List<int> cells = new List<int> { 1,  2,  3,  4,  
                                              5,  6,  7,  8,  
                                              9, 10, 11, 12, 
                                             13, 14, 15, 16};

            var box = GetBoxCells<int>(cells, position, width, boxWidth, height, boxHeight);

            Assert.IsNotNull(box);
            Assert.AreEqual(boxWidth * boxHeight, box.Count);
        }

        [TestMethod]
        [Ignore]
        public void BoxLogicSixBySixTest()
        {
            int position = 4;
            int width = 6;
            int height = 6;
            int boxWidth = 2;
            int boxHeight = 2;
            List<int> cells = new List<int> { 1,  2,  3,  4, 5,  6,  
                                              7,  8,  9, 10, 11, 12, 
                                             13, 14, 15, 16, 17, 18,
                                             19, 20, 21, 22, 23, 24,
                                             25, 26, 27, 28, 29, 30,
                                             31, 32, 33, 34, 35, 36};

            var box = GetBoxCells<int>(cells, position, width, boxWidth, height, boxHeight);

            Assert.IsNotNull(box);
            Assert.AreEqual(boxWidth * boxHeight, box.Count);
        }
        #endregion Box Logic Tests
        
        #endregion Tests
    }
}
